; ----------------
; KRISIS Play Platform Makefile 
; Permanent URL: https://bitbucket.org/cpmadmin/krisis-play-platform.git
; Build Version: v0.002
; Build Name: build_0_002
; Build Description: Initial Repack of the demo install
; Filename: krisisplay.make
; ----------------  

  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
api = 2
      
; Modules
; --------

projects[media][subdir] = "contrib"
projects[media][version] = "1.3"
projects[media][type] = "module"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.x-dev"
projects[jquery_update][type] = "module"

projects[ctools][subdir] = "contrib"
projects[ctools]][version] = "2.x-dev"
projects[ctools]][type] = "module"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"
projects[views][type] = "module"

projects[navigation404][subdir] = "contrib"
projects[navigation404][version] = "1.0"
projects[navigation404][type] = "module"

projects[simplehtmldom][subdir] = "contrib"
projects[simplehtmldom][version] = "1.12"
projects[simplehtmldom][type] = "module"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"
projects[token][type] = "module"

projects[md_slider][subdir] = "contrib"
projects[md_slider][type] = "module"
projects[md_slider][download][type] = "git"
projects[md_slider][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-mdslider.git"
projects[md_slider][download][branch] = "build_0_001"

projects[chat][subdir] = "contrib"
projects[chat][type] = "module"
projects[chat][download][type] = "git"
projects[chat][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-123flashchatdrupal.git"
projects[chat][download][branch] = "build_0_001"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"
projects[devel][type] = "module"

projects[features][subdir] = "contrib"
projects[features][version] = "2.0-beta1"
projects[features][type] = "module"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"
projects[libraries][type] = "module"

projects[color_field][version] = "1.6"
projects[color_field][subdir] = "contrib"
projects[color_field][type] = "module"

projects[conditional_fields][type] = "module"
projects[conditional_fields][version] = "3.x-dev"
projects[conditional_fields][subdir] = "contrib"

projects[multiblock][subdir] = "contrib"
projects[multiblock][version] = "1.1"
projects[multiblock][type] = "module"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"
projects[pathauto][type] = "module"

projects[contact_form_blocks][version] = "1.x-dev"
projects[contact_form_blocks][subdir] = "contrib"
projects[contact_form_blocks][type] = "module"

projects[devel_themer][subdir] = "contrib"
projects[devel_themer][version] = "1.x-dev"
projects[devel_themer][type] = "module"

projects[robotstxt][subdir] = "contrib"
projects[robotstxt][version] = "1.1"
projects[robotstxt][type] = "module"

projects[entitycache][subdir] = "contrib"
projects[entitycache][version] = "1.1"
projects[entitycache][type] = "module"

projects[themekey][subdir] = "contrib"
projects[themekey][version] = "2.5"
projects[themekey][type] = "module"


; Libraries
; ---------

; jQuery Colorpicker
libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][download][type] = "git"
libraries[colorpicker][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-colourpicker.git"
libraries[colorpicker][download][branch] = "build_0_001"


; jQuery Colorpicker
libraries[bgrins-spectrum][directory_name] = "bgrins-spectrum"
libraries[bgrins-spectrum][type] = "library"
libraries[bgrins-spectrum][destination] = "libraries"
libraries[bgrins-spectrum][download][type] = "git"
libraries[bgrins-spectrum][download][url] = "https://github.com/bgrins/spectrum.git"



